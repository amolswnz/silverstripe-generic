<?php

class MetaSiteConfigExtension extends DataExtension {

	private static $db = array(
		'TwitterID' => 'Varchar(255)',
		'CompanyName' => 'Varchar(255)',
		'WebsiteName' => 'Varchar(255)', //The preferred name of your website.
		'AlternateWebsiteName' => 'Varchar(255)',
		'SitelinksSearchboxUrl' => 'Varchar(255)', //https://query.example.com/search?q={search_term_string},
	);

	private static $has_one = array(
		'MetaImage' => 'Image',
		'Logo' => 'Image'
	);

    public function updateCMSFields(FieldList $fields) {
	    $fields->addFieldToTab('Root.Social', TextField::create('TwitterID', 'Twitter ID (used for twitter cards meta)'));

	    $fields->addFieldToTab('Root.Meta', UploadField::create('MetaImage', 'Default Meta Image'));
	    $fields->addFieldToTab('Root.Main', TextField::create('CompanyName', 'Your company name')->setRightTitle('To appear in Google places info.'));
	    $fields->addFieldToTab('Root.Main', TextField::create('WebsiteName', 'Your website name')->setRightTitle('To appear in Google places info.'));
	    $fields->addFieldToTab('Root.Main', UploadField::create('Logo', 'Your company logo'));

	    $fields->addFieldToTab('Root.Meta', TextField::create('AlternateWebsiteName', 'Alternative Website Name')->setRightTitle('An alternate name you want Google to consider.'));
	    $fields->addFieldToTab('Root.Meta', TextField::create('SitelinksSearchboxUrl', 'URL to use for Google sitelinks searchbox')->setRightTitle('i.e. https://your_domain/search?q={search_term_string}'));

	    $fields->addFieldToTab('Root.Meta', GridField::create('CorporateContacts', 'Contact Details', MetaCorporateContact::get(), $this->get_gridfieldconfig()));
    }

	protected function get_gridfieldconfig() {
		$gridFieldConfig = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldAddNewButton('toolbar-header-right'),
			new GridFieldDataColumns(),
			new GridFieldPaginator(20),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm()
		);
		return $gridFieldConfig;
	}
}
