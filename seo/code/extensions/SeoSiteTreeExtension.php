<?php

/**
 * Created by PhpStorm.
 * User: hothouse
 * Date: 19/05/16
 * Time: 1:55 PM
 */
class SeoSiteTreeExtension extends DataExtension {

	private static $db = array(
		'H1' => 'Varchar(255)'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->insertAfter( TextField::create('H1', 'H1 Content (if empty use Name above)'), 'Title');
	}

	public function H1() {
		return trim($this->owner->H1 ? $this->owner->H1 : $this->owner->Title);
	}

	public function contentcontrollerInit($controller) {
		$siteConfig = SiteConfig::current_site_config();

		if ($breadCrumbs = $this->getGoogleMetaBreadCrumbs($siteConfig)) {
			Requirements::insertHeadTags($breadCrumbs);
		}
		if ($websiteMeta = $this->getGoogleMetaWebsite($siteConfig)) {
			Requirements::insertHeadTags($websiteMeta);
		}
		if ($organizationMeta = $this->getGoogleMetaOrganization($siteConfig)) {
			Requirements::insertHeadTags($organizationMeta);
		}
	}

	protected function getGoogleMetaOrganization($siteConfig) {
		$metaProperties = new stdClass();
		$metaProperties->{'@context'} = 'http://schema.org';
		$metaProperties->{'@type'} = 'Organization';

		$siteConfig = SiteConfig::current_site_config();

		$metaProperties->url = Director::absoluteBaseURL();
		if (trim($siteConfig->CompanyName)) {
			$metaProperties->name = $siteConfig->CompanyName;
		}
		if (trim($siteConfig->Logo())) {
			$metaProperties->logo = $siteConfig->Logo()->getAbsoluteURL();
		}
		$corporateContact = array();
		foreach (MetaCorporateContact::get() as $contact) {
			$tmpValues = new stdClass();
			$tmpValues->telephone = $contact->Telephone;
			$tmpValues->contactType = $contact->ContactType;

			if ($contact->AreaServed) {
				$tmpValues->areaServed = count(explode(',', $contact->AreaServed)) > 1 ? explode(',', $contact->AreaServed) : $contact->AreaServed;

			}
			if ($contact->ContactOption) {
				$tmpValues->contactOption = $contact->ContactOption;
			}
			if ($contact->AvailableLanguage) {
				$tmpValues->availableLanguage = count(explode(',', $contact->AvailableLanguage)) > 1 ? explode(',', $contact->AvailableLanguage) : $contact->AvailableLanguage;
			}
			$tmpValues->{'@type'} = 'ContactPoint';
			$corporateContact[] = $tmpValues;
		}
		if (count($corporateContact)) {
			$metaProperties->contactPoint = $corporateContact;
		}

		$socialServices = array();
		$socialServicesAllowed = array (
			'FacebookURL',
			'TwitterURL',
			'GooglePlusURL',
			'InstagramURL',
			'YouTubeURL',
			'LinkedInURL',
			'MyspaceURL',
			'PinterestURL',
			'SoundCloudURL',
			'TumblrURL'
		);
		foreach ($socialServicesAllowed as $serviceName) {
			if (trim($siteConfig->$serviceName)) {
				$socialServices[] = $siteConfig->$serviceName;
			}
		}
		if (count($socialServices)) {
			$metaProperties->sameAs = $socialServices;
		}

		return '<script type="application/ld+json">' . json_encode($metaProperties) . '</script>';
	}

	protected function getGoogleMetaWebsite($siteConfig) {
//		{
//			"@context": "http://schema.org",
//          "@type": "WebSite",
//          "name": "WEBSITE_NAME",
//          "alternateName": "ALTERNATE_WEBSITE_NAME",
//          "url": "http://WEBSITE_URL",
//          "potentialAction": {
//			   "@type": "SearchAction",
//             "target": "https://query.example.com/search?q={search_term_string}",
//             "query-input": "required name=search_term_string"
//          }
//      }

		$metaWebsite = new stdClass();
		$metaWebsite->{'@context'} = "http://schema.org";
		$metaWebsite->{'@type'} = "WebSite";
		$metaWebsite->url = Director::absoluteBaseURL();

		if ($siteConfig->WebsiteName) {
			$metaWebsite->name = $siteConfig->WebsiteName;
		}

		if ($siteConfig->AlternateWebsiteName) {
			$metaWebsite->alternateName = $siteConfig->AlternateWebsiteName;
		}

		if ($siteConfig->SitelinksSearchboxUrl) {
			$metaWebsite->potentialAction = new stdClass();
			$metaWebsite->potentialAction->{'@type'} = 'SearchAction';
			$metaWebsite->potentialAction->{'query-input'} = 'required name=search_term_string';
			$metaWebsite->potentialAction->target = $siteConfig->SitelinksSearchboxUrl;
		}

		return '<script type="application/ld+json">' . json_encode($metaWebsite) . '</script>';
	}

	protected function getGoogleMetaBreadCrumbs($siteConfig) {
		$crumbs = array();
		$ancestors = array_reverse($this->owner->getAncestors()->toArray());
		foreach($ancestors as $ancestor) $crumbs[] = $ancestor;
		$crumbs[] = $this->owner;

		$breadMeta = new stdClass();
		$breadMeta->{'@context'} = "http://schema.org";
		$breadMeta->{'@type'} = "BreadcrumbList";
		$breadMeta->itemListElement = array();

		$counter = 1;
		foreach ($crumbs as $crumb) {
			$tmpObject = new stdClass();
			$tmpObject->{'@type'} = "ListItem";
			$tmpObject->position = $counter;
			$tmpObject->item = new stdClass();
			$tmpObject->item->{'@id'} = $crumb->AbsoluteLink();
			$tmpObject->item->name = $crumb->MenuTitle;
			if ($crumb->MetaImageID) {
				$tmpObject->item->image = $crumb->MetaImage()->getAbsoluteURL();
			}
			$counter++;
			$breadMeta->itemListElement[] = $tmpObject;
		}

		return '<script type="application/ld+json">' . json_encode($breadMeta) . '</script>';

	}
}

