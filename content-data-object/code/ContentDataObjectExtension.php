<?php

class ContentDataObjectExtension extends DataExtension {

	function canView($member = NULL) {
		return true;//(Permission::check('CMS_ACCESS_CMSMain') || Permission::check('ADMIN'));
	}

	function canEdit($member = NULL) {
		return (Permission::check('CMS_ACCESS_CMSMain') || Permission::check('CMS_ACCESS_LeftAndMain') || Permission::check('ADMIN'));
	}

	function canCreate($member = NULL) {
		return (Permission::check('CMS_ACCESS_CMSMain') || Permission::check('CMS_ACCESS_LeftAndMain') || Permission::check('ADMIN'));
	}

	function canDelete($member = NULL) {
		return (Permission::check('CMS_ACCESS_CMSMain') || Permission::check('CMS_ACCESS_LeftAndMain') || Permission::check('ADMIN'));
	}

}





