<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 28/10/15
 * Time: 12:18 PM
 */
class StringfieldExtension extends Extension {

	public function ENTITIES() {
		$result = $this->owner;
		$val = (is_object($result) && $result instanceof Object) ? $result->forTemplate() : $result;

		if (is_array($val)) {
			foreach ($val as $k => $v) {
				$val[$k] = htmlentities($v);
			}
			$return = $val;
		} else {
			$return = htmlentities($val);
		}

		return $return;
	}

	public function URLENC() {
		$result = $this->owner;
		$val = (is_object($result) && $result instanceof Object) ? $result->forTemplate() : $result;

		if (is_array($val)) {
			foreach ($val as $k => $v) {
				$val[$k] = urlencode($v);
			}
			$return = $val;
		} else {
			$return = urlencode($val);
		}

		return $return;
	}

	public function HTMLATTRIBUTE() {
		$result = $this->owner;
		$val = (is_object($result) && $result instanceof Object) ? $result->forTemplate() : $result;

		if (is_array($val)) {
			foreach ($val as $k => $v) {
				$val[$k] = Convert::raw2url($v);
			}
			$return = $val;
		} else {
			$return = Convert::raw2url($val);
		}

		return $return;
	}

	public function ALLOWTAGS() {
		$value = trim($this->owner->RAW());
		$matches = array();
		$sep = '###HTMLTAG###';
		preg_match_all(":</{0,1}[a-z]+[^>]*>:i", $value, $matches);
		$tmp = preg_replace(":</{0,1}[a-z]+[^>]*>:i", $sep, $value);
		$tmp = explode($sep, $tmp);
		for ($i = 0; $i < count($tmp); $i++) {
			$tmp[$i] = htmlentities($tmp[$i], ENT_QUOTES, 'UTF-8', false);
		}
		$tmp = implode($sep, $tmp);
		for ($i = 0; $i < count($matches[0]); $i++) {
			$tmp = preg_replace(":$sep:", $matches[0][$i], $tmp, 1);
		}
		return $tmp;
	}

	public function PHONE() {
		$result = $this->owner;
		$val = (is_object($result) && $result instanceof Object) ? $result->forTemplate() : $result;

		if (is_array($val)) {
			foreach ($val as $k => $v) {
				$val[$k] = $this->adjustPhoneString($v);
			}
			$return = $val;
		} else {
			$return = $this->adjustPhoneString($val);
		}

		return $return;
	}

	protected function adjustPhoneString($string) {
		$string = preg_replace('/[^0-9+]/', '', $string);
		if (substr($string, 0, 1) != '+') {
			if (substr($string, 0, 1) == '0') {
				$string = substr($string, 1);
			}
			$string = '+64'.$string;
		}

		return $string;
	}

}