<?php

class HoneypotSpamProtectorField extends FormField {

	const FORM_FIELD_NAME = 'hcaptchahcaptchahcaptcha';
	const FORM_FIELD_NAME_TIMER = 'hcaptchahcaptchahcaptchatimer';
	const FORM_FIELD_LABEL = 'Captcha - please don\'t enter anything here';

	public function Field($properties = array()) {
		$honeypot = CompositeField::create(
			new TextField($this->name.self::FORM_FIELD_NAME, self::FORM_FIELD_LABEL, ''),
			new HiddenField($this->name.self::FORM_FIELD_NAME_TIMER, null, time())
		);
		$honeypot->addExtraClass('hidden');
		return $honeypot;
	}

	public function FieldHolder($properties = array()) {
		return $this->Field($properties);
	}

	public function validate($validator) {
		$Request = $this->getForm()->getRequest();
		$submittedFieldContent = $Request->requestVar($this->name.self::FORM_FIELD_NAME);
		$timerContent = intval($Request->requestVar($this->name.self::FORM_FIELD_NAME_TIMER));

		if (
			$submittedFieldContent !== ''
			|| $timerContent == 0
			|| (time() - $timerContent) < 3
			|| (time() - $timerContent) > 90000) {
			$validator->validationError(
				$this->name,
				'Sorry we are unable to process this. Please send us an email with your request.',
				"validation",
				false
			);

			return false;
		}

		return true;
	}

	public function Type() {
		return '';
	}

}