<?php

namespace FormsSpamFilter;

class FormsSpamFilter_ControllerExtension extends \DataExtension {

	// extends the form fields on userdefinedform_controller
	public function updateFormFields(&$fields) {
		$fields->push(\HoneypotSpamProtectorField::create('Captcha'));
	}

}
